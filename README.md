Packer Arch
===========

**La documentation complète est sur le git de reference** : https://github.com/elasticdog/packer-arch.git

Il s'agit d'une adaptation pour correspondre à MON besoin.

Il y a 5 FS par défaut, avec LVM :
- / de 10Go
- /home de 1Go
- /var de 512M
- /var/cache/pacman de 5Go

Le disque par defaut fait 32Go et se trouve dans un vg

Usage
-----

    $ git clone https://gitlab.com/mhamon/packer-arch.git
    $ cd packer-arch/
    $ packer build -only=[PROVIDER]-iso arch-template.json

    $ vagrant box add arch output/packer_arch_[PROVIDER].box
