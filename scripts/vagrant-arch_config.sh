#! /usr/bin/env bash
echo 'vagrant-arch' > /etc/hostname
/usr/bin/ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime
echo 'KEYMAP=fr-latin9' > /etc/vconsole.conf
echo 'FONT=lat9w-16' >> /etc/vconsole.conf
echo 'LANG=fr_FR.UTF-8' > /etc/locale.conf
echo 'LC_COLLATE=C' >> /etc/locale.conf
/usr/bin/sed -i 's/#fr_FR.UTF-8/fr_FR.UTF-8/' /etc/locale.gen
/usr/bin/locale-gen
/usr/bin/sed -i '/^HOOKS/c\HOOKS=(base systemd autodetect keyboard sd-vconsole modconf block lvm2 filesystems fsck)' /etc/mkinitcpio.conf
/usr/bin/mkinitcpio -p linux
/usr/bin/echo -n "root:vagrant" | chpasswd -s 65536 -c SHA512
/usr/bin/sed -i 's/^#PermitRootLogin.*/PermitRootLogin yes/g' /etc/ssh/sshd_config
/usr/bin/systemctl enable dhcpcd.service
/usr/bin/systemctl enable sshd.service
/usr/bin/pacman-key --populate
# Provide /run/lvm access to chroot environnement -- https://bugs.archlinux.org/task/61040
/usr/bin/ln -s /hostlvm /run/lvm
/usr/bin/sed -i -re "s/^GRUB_TIMEOUT=.*/GRUB_TIMEOUT=\"2\"/" /etc/default/grub
/usr/bin/grub-mkconfig -o /boot/grub/grub.cfg
[ -d /boot/grub/themes ] && /usr/bin/rm -rf /boot/grub/themes
/usr/bin/echo "Vagrant specific configuration"
/usr/bin/sed -i -re 's/#UseDNS.*/UseDNS no/' /etc/ssh/sshd_config
# Vagrant default password not need to encrypt
/usr/bin/useradd --comment 'Vagrant User' --create-home --user-group vagrant
/usr/bin/echo -n "vagrant:vagrant" | chpasswd -s 65536 -c SHA512
/usr/bin/echo 'Defaults env_keep += "SSH_AUTH_SOCK"' > /etc/sudoers.d/10_vagrant
/usr/bin/echo 'vagrant ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers.d/10_vagrant
/usr/bin/chmod 0440 /etc/sudoers.d/10_vagrant
/usr/bin/install --directory --owner=vagrant --group=vagrant --mode=0700 /home/vagrant/.ssh
/usr/bin/curl --output /home/vagrant/.ssh/authorized_keys --location https://raw.github.com/mitchellh/vagrant/master/keys/vagrant.pub
/usr/bin/chown vagrant:vagrant /home/vagrant/.ssh/authorized_keys
/usr/bin/chmod 0600 /home/vagrant/.ssh/authorized_keys
# Virtualbox packages will be automatically installed
/usr/bin/echo -e 'vboxguest\nvboxsf\nvboxvideo' > /etc/modules-load.d/virtualbox.conf
/usr/bin/systemctl enable vboxservice.service
/usr/bin/systemctl enable rpcbind.service
# Add groups for Virtualbox folder sharing
/usr/bin/usermod --append --groups vagrant,vboxsf vagrant
