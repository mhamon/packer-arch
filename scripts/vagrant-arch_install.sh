#! /usr/bin/env bash
echo "==> Launch Installation in BIOS mode"
echo "==> Check /.install directory existence"
[ -d "/.install" ] && echo "/.install already exist, cannot continue" && exit 1
echo "==> Update localtime"
/usr/bin/ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime
echo "==> Update system clock"
/usr/bin/timedatectl set-ntp true
echo "==> Setting local mirror"
/usr/bin/curl -s "https://archlinux.org/mirrorlist/?country=FR&protocol=http&protocol=https&ip_version=4&use_mirror_status=on" | sed 's/^#Server/Server/' > /etc/pacman.d/mirrorlist

# DESTROY PARTITIONS
echo "==> Clearing partition table on /dev/sda"
/usr/bin/sgdisk --zap /dev/sda
echo "==> Destroying magic strings and signatures on /dev/sda"
/usr/bin/dd if=/dev/zero of=/dev/sda bs=512 count=2048
/usr/bin/wipefs --all /dev/sda

# CREATE PARTITIONS
echo "==> Creating partitions on /dev/sda"
/usr/bin/sgdisk --new=0:0:+4M --typecode=0:ef02 --new=0:0:+512M --typecode=0:ef00 --new=0:0:0 --typecode=0:8e00 /dev/sda
echo "==> Setting /dev/sda bootable"
/usr/bin/sgdisk /dev/sda --attributes=1:set:2
echo "==> Creating /boot filesystem (EXT4)"
/usr/bin/mkfs.ext4 -O ^64bit -F -m 0 -q -L boot /dev/sda2

# LVM
echo "==> Creating LVM"
/usr/bin/pvcreate /dev/sda3
/usr/bin/vgcreate vg_arch /dev/sda3
/usr/bin/lvcreate -y vg_arch --name lv_root --size 10G && /usr/bin/mkfs.ext4 /dev/mapper/vg_arch-lv_root
/usr/bin/lvcreate -y vg_arch --name lv_pacman --size 5G && /usr/bin/mkfs.ext4 /dev/mapper/vg_arch-lv_pacman

echo "==> Mounting partitions to /mnt"
/usr/bin/mount /dev/vg_arch/lv_root /mnt
/usr/bin/mkdir -p /mnt//boot
/usr/bin/mount /dev/sda2 /mnt//boot
/usr/bin/mkdir -p /mnt/var/cache/pacman && /usr/bin/mount /dev/vg_arch/lv_pacman /mnt/var/cache/pacman

# BOOTSTRAP
echo "==> Bootstrapping the base installation"
/usr/bin/pacstrap /mnt base linux lvm2 dhcpcd openssh grub-bios vi sudo linux-headers virtualbox-guest-utils nfs-utils

# GRUB
echo "==> GRUB installation on /dev/sda"
/usr/bin/arch-chroot /mnt grub-install --no-floppy --target=i386-pc --recheck /dev/sda
echo "==> GRUB Config"
## Provide /run/lvm access to chroot environnement -- https://bugs.archlinux.org/task/61040
/usr/bin/mkdir -p /mnt/hostlvm
/usr/bin/mount --bind /run/lvm /mnt/hostlvm

# FSTAB
echo "==> Generating the filesystem table"
/usr/bin/genfstab -p /mnt >> "/mnt/etc/fstab"

# SYSTEM CONFIG
echo "==> Generating the system configuration script"
# Script must be copy by packer to /tmp
/usr/bin/install --mode=0755 /tmp/vagrant-arch_config.sh "/mnt/vagrant-arch_config.sh"
echo "==> Entering chroot and configuring system"
/usr/bin/arch-chroot /mnt /vagrant-arch_config.sh
rm "/mnt/vagrant-arch_config.sh"

# END
echo "==> That's all folks !"

# REBOOT
read -r -p "Reboot (y/N) ? " _reboot
_reboot="$(echo "$_reboot" | tr '[:upper:]' '[:lower:]')"
if [ "$_reboot" == "y" ]; then
  /usr/bin/sleep 3
  /usr/bin/umount --recursive /mnt
  /usr/bin/systemctl reboot
fi
