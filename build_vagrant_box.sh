#!/usr/bin/env bash

export _packer="packer"
export _provider="virtualbox"
export _release=""
export _buildConfFile="arch-template.json"
export _buildOutputDir="output"
export _buildOutputFile="arch_${_provider}.box"
export _boxName="arch"

_checkPackerInstallation() {

  if ! which "$_packer" > /dev/null 2>&1; then
    echo "Packer not install"
    exit 1
  fi

}

_buildBox() {

  $_packer build -only=${_provider}-iso ${_buildConfFile}
  export _rc=$?

}

_endBuild() {

  if [ $_rc -eq 0 ]; then
    echo -e "\nAdd box to vagrant : vagrant box add ${_boxName} ${_buildOutputDir}/${_buildOutputFile}\n"
    exit 0
  fi

}

_checkPackerInstallation
_buildBox
_endBuild

exit $_rc
